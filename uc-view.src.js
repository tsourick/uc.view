
UC = {
	$   : (qs, from = document) => from.querySelectorAll(qs),
	$0  : (qs, from = document) => from.querySelector(qs),
	$id : (id, from = document) => from.querySelector(`*[data-id=${id}]`)
};


UC.Utils = class
{
	/**
	* returns class by string with namespaces or the object itself
	*/
	static getClass(name)
	{
		let c = false;

		if (_.isString(name))
		{
			let names = name.split('.');

			c = window;
			names.forEach((name) => {
				c = c[name] ?? false;
			});
		}
		else if (_.isObject(name) || _.isFunction(name)) c = name;
		
		return c;
	}
}


UC.Base = class
{
	inst(cls, ...args)
	{
		let c = UC.Utils.getClass(cls);
		
		if (false === c) throw `class '${cls}' not found`;	

		return new c(...args);
	}
}

UC.Element = class extends UC.Base
{
	root;
	o = {};
	
	constructor(root, o = {})
	{
		super();
		
		this.root = root;

		this.o = _.merge({}, this.o, o);
	}
	
	$(qs)    { return UC.$(qs, this.root); }
	$0(qs)   { return UC.$0(qs, this.root); }
	$id(id)  { return UC.$id(id, this.root); }

	show(show)
	{
		this.root.hidden = !show;
	}

	addListener(el, eName, l = this.onClick)
	{
		el.addEventListener(eName, l.bind(this));
	}

	addClickListener(el, l = this.onClick)
	{
		this.addListener(el, 'click', l);
	}
};


UC.ElementWithEvents = class extends UC.Element
{
	allowUserEvents = true; // set to false on prog events
	

	/**
	* @param  alias  can be ('onEvent'|'(E|e)vent');  will be transformed to 'onEvent'
	*/
	emit(alias, args = [])
	{
		let lalias = alias.toLowerCase(), ualias = _.upperFirst(alias);
		
		alias = lalias.substr(0, 2) == 'on' ? alias : 'on' + ualias;
			
		return this.o[alias].apply(this, args);
	}
	
	/**
	* @param  alias  can be ('onUserEvent'|'(U|u)serEvent'|'(E|e)vent');  will be transformed to 'onUserEvent'
	*/
	userEmit(alias, args = [])
	{
		let lalias = alias.toLowerCase(), ualias = _.upperFirst(alias);
		
		alias = lalias.substr(0, 2) == 'on' ? alias
		: (
			lalias.substr(0, 4) == 'user' ? 'on' + ualias :  'onUser' + ualias
		);
			
		if (this.allowUserEvents) return this.o[alias].apply(this, args);
	}
};


UC.View = class extends UC.ElementWithEvents
{
};


UC.Cmp = class extends UC.ElementWithEvents
{
	updateView(html)
	{
		this.root.innerHTML = html;
	}
};

UC.CmpOfSelectableItems = class extends UC.Cmp // single selection supported only
{
	items;
	o = _.merge({}, this.o, {
		itemSelector: '.items > *',
		selectedClass: 'selected',
		
		allowReselect: false // select already selected
	});
	data = {
		selected: null // selected item
	};
	
	
	reset()
	{
		this.items.forEach((item) => {
			let cl = item.classList, cn = this.o.selectedClass;
			cl.remove(cn);
		});
		
		this.data.selected = null;
	}
	

	parseView()
	{
		this.items = this.$(this.o.itemSelector);
		
		this.items.forEach((item) => {
			item.addEventListener('click', this.onItemClick.bind(this));
		});		

		this.data.selected = this.$0(this.o.itemSelector + '.' + this.o.selectedClass);
	}
	
	init()
	{
		this.parseView();
	}
	
	itemClick(e) // protected, override in derived classes
	{
		this.items.forEach((item) => {
			let cl = item.classList, cn = this.o.selectedClass;
			(item == e.target) ? (cl.add(cn), this.data.selected = item) : cl.remove(cn);
		});
	}
	
	onItemClick(e)
	{
		if (this.o.allowReselect || this.data.selected != e.target) {
			this.itemClick(e);
		}
	}
};

UC.CmpSorter = class extends UC.CmpOfSelectableItems
{
	o = _.merge({}, this.o, {
		onUserChange:  () => {},
		
		nameGetter: (el) => el.getAttribute('data-name'),
		
		dirGetter: (el) => el.classList.contains('desc') ? 'desc' : 'asc',
		dirSetter: (el, dir) => { let cl = el.classList; cl.remove('asc', 'desc'); cl.add(dir); },
		dirEraser: (el) => { el.classList.remove('asc', 'desc') }
	});
	data = _.merge({}, this.data, {
		sorting: null // {name, order}
	});

	
	constructor(root, o = {})
	{
		super(root, o);
		
		// Object.assign(this.o, o); 
		this.o = _.merge({}, this.o, o); // must redo, because defaults applied after super(), weird
	}

	
	parseView()
	{
		super.parseView();

		let el;
		if (el = this.data.selected) {
			let name = this.o.nameGetter(el) || (() => { throw 'No sorting name set'; })();
			let dir = this.o.dirGetter(el);
			
			this.o.dirSetter(el, dir); // ensure default dir set
			
			this.data.sorting = {name, dir};
		}
	}

	
	toggleDir(dir)
	{
		return dir == 'desc' ? 'asc' : 'desc';
	}
	
	itemClick(e)
	{
		// n/a
	}
	
	onItemClick(e)
	{
		let el = e.target;

		let newName = this.o.nameGetter(el);
		let newDir = this.data.selected != e.target ? 'asc' : this.toggleDir(this.o.dirGetter(el));
		
		this.items.forEach((item) => {
			let cl = item.classList, cn = this.o.selectedClass;
			
			(item == e.target)
				? (
						cl.add(cn), this.o.dirSetter(item, newDir),
						this.data.selected = item,
						this.data.sorting = {name: newName, dir: newDir}
					)
				: (cl.remove(cn), this.o.dirEraser(item));
		});
		
		this.userEmit('change', [this.data.sorting]);
	}
};

UC.CmpPager = class extends UC.CmpOfSelectableItems
{
	o = _.merge({}, this.o, {
		onUserChange:  () => {},
		pageAttribute: 'data-page'
	});
	data = _.merge({}, this.data, {
		page: null // selected page
	});

	
	constructor(root, o = {})
	{
		super(root, o);
		
		// Object.assign(this.o, o); 
		this.o = _.merge({}, this.o, o); // must redo, because defaults applied after super(), weird
	}


	reset()
	{
		super.reset();
		
		this.data.page = null;
	}
	

	readPage()
	{
		let item, page = (item = this.data.selected) ? item.getAttribute(this.o.pageAttribute) : null;
		this.data.page = page;
	}
	
	parseView()
	{
		super.parseView();

		this.readPage();
	}
		
	itemClick(e)
	{
		super.itemClick(e);
		
		this.readPage();
		
		this.userEmit('change', [this.data.page]);
	}
};

UC.CmpSelected = class extends UC.Cmp
{
	o = _.merge({}, this.o, {
		itemSelector: ':scope > div:not(.reset)',
		resetSelector: ':scope > div.reset',
		onUserRemove:  () => {},
		onUserReset:   () => {},
		onAdd:         () => {},
		onRemove:      () => {},
		onReset:       () => {}
	});
	data = {
	};

	
	constructor(root, o = {})
	{
		super(root, o);
		
		// Object.assign(this.o, o); 
		this.o = _.merge({}, this.o, o); // must redo, because defaults applied after super(), weird
	}
	
	
	showhide()
	{
		this.show(this.$(this.o.itemSelector).length > 0);
	}

	add(id, title)
	{
		let el = this.$id(id), update;
		
		if (el) // replace existing
		{
			update = true;
			
			el.innerText = title;
		}
		else // add new
		{
			update = false;
			
			// this.root.innerHTML += `<div data-id="${id}">${title}</div>`;
			el = document.createElement('div');
			el.setAttribute('data-id', id);
			el.innerText = title;
			this.root.appendChild(el);
			
			// el = this.$id(id);
			this.addClickListener(el, this.onUserRemove);
			
			this.show(true);
		}

		this.emit('add', [id, title, update]);
	}
	
	remove(id)
	{
		let el = this.$0(this.o.itemSelector + '[data-id=' + id + ']');
		
		el.remove();
		
		this.showhide();
		
		this.emit('remove', [id, el]);
	}

	onUserRemove(e)
	{
		let el = e.target;
		var id = el.getAttribute('data-id');
		
		this.userEmit('remove', [id, el]);
		this.emit('remove', [id, el]);
	}

	reset()
	{
		this.$(this.o.itemSelector).forEach((item) => { item.remove(); });

		this.show(false);

		this.emit('reset');
	}

	onUserReset()
	{
		this.userEmit('reset');
		this.emit('reset');
	}

	
	parseView()
	{
	}
	
	init()
	{
		let reset = this.$0(this.o.resetSelector);
		
		this.addClickListener(reset, this.onUserReset);
		
		
		let items = this.$(this.o.itemSelector);
		
		items.forEach((item) => { this.addClickListener(item, this.onUserRemove); });
		
		
		this.parseView();
		
		
		this.showhide();
	}
};

UC.CmpFilter = class extends UC.Cmp
{
	o = _.merge({}, this.o, {
		onUserChange: () => {},
		onUserSubmit: () => {},
		onUserReset: () => {},
		onChange: () => {},
		onSubmit: () => {},
		onReset: () => {}
	});
	data = {
		fd: null, // FormData
		sp: null, // URLSearchParams
		q: null   // URLSearchParams.toString()
	};
	
	constructor(root, o = {})
	{
		super(root, o);
		
		// Object.assign(this.o, o); 
		this.o = _.merge({}, this.o, o); // must redo, because defaults applied after super(), weird
	}
	
	
	onSubmit(e)
	{
		e.preventDefault();
		
		this.userEmit('submit');
		this.emit('submit');
	}
	
	onReset(e)
	{
		e.preventDefault();
		
		this.userEmit('reset');
		this.emit('reset');
	}
	
	onChangeEl(el)
	{
		this.parseView();

		this.userEmit('change', [el]);
		this.emit('change', [el]);
	}
	
	onChange(e)
	{
		let el = e.target;
		
		this.onChangeEl(el);
	}
	
	unsetElCheckbox(el) { el.checked = false; }
	unsetElRadio(el) { el.checked = false; }
	unsetElText(el) { el.value = ''; }
	unsetElSelectOne(el) {
		let emptyIndex = Array.from(el.options).findIndex(option => option.value == '');
		el.selectedIndex = emptyIndex; // select empty value index or -1 if none
	}
	
	getElType(el)
	{
		return el.type;
	}
	
	unsetEl(el) // override in derived classes
	{
		let type = this.getElType(el);
		
		switch (type)
		{
			case 'text':        this.unsetElText(el);       break;
			case 'checkbox':    this.unsetElCheckbox(el);   break;
			case 'radio':       this.unsetElRadio(el);      break;
			case 'select-one':  this.unsetElSelectOne(el);  break;
			default:
				console.log(`Unsupported input type '${el.type}'`);
		}
	}

	findEl(id)
	{
		return this.$0(`input[id=${id}],select[id=${id}],input[type=radio][name=${id}]:checked`);
	}
	
	unset(id)
	{
		let el = this.findEl(id);

		if (el)
		{
			this.allowUserEvents = false;

			this.unsetEl(el);
			
			this.onChangeEl(el);
			
			this.allowUserEvents = true;
		}
	}

	clear()
	{
		/*
		_self.jForm.find('input[type=checkbox]').prop('checked', false);
		_self.jForm.find('input[type=text]').val('');
		_self.jForm.find('.xbcms-controls-ddlist').each((index, el) => { el.ddList.reset() });
		
		_self.jForm
		.find('> dl > dt').removeClass('set')
		.find('> b').html('0').hide(); // reset counts
		*/
		this.$('input[type=text]').forEach(this.unsetElText);
		this.$('input[type=checkbox]').forEach(this.unsetElCheckbox);
		this.$('input[type=radio]').forEach(this.unsetElRadio);
		this.$('select:not([multiple])').forEach(this.unsetElSelectOne);
	}
	
	reset()
	{
		this.clear();
		
		this.parseView();
		
		this.emit('reset');
	}

	parseViewByStage(stage)
	{
		switch (stage)
		{
			case 'fd':
				this.data.fd = new FormData(this.root);
				// console.log('fd.entries', [...this.data.fd.entries()]);
			break;
			case 'sp':
				this.data.sp = new URLSearchParams(this.data.fd);
				// console.log('sp.entries', [...this.data.sp.entries()]);
			break;
			case 'q':
				this.data.q = this.data.sp.toString();
				// console.log('query (SP):', this.data.q);
			break;
		}
	}
	
	parseView()
	{
		this.parseViewByStage('fd');
		this.parseViewByStage('sp');
		this.parseViewByStage('q');
	}
	
	init()
	{
		this.addListener(this.root, 'submit', this.onSubmit);
		this.addListener(this.root, 'reset', this.onReset);
		
		this.$('input,select').forEach((item) => {
			this.addListener(item, 'change', this.onChange);
		});

		
		this.parseView();
	}
};

UC.CmpContent = class extends UC.Cmp
{
	constructor(root, o = {})
	{
		super(root, o);
		
		// Object.assign(this.o, o); 
		this.o = _.merge({}, this.o, o); // must redo, because defaults applied after super(), weird
	}
	
	parseView()
	{
	}
	
	init()
	{
		this.parseView();
	}
};


UC.ViewBasic = class extends UC.View
{
	o = _.merge({}, this.o, {
		classes: {
			sorter:   {name: 'UC.CmpSorter'  , sel: '#cmp-sorter'  },
			pager:    {name: 'UC.CmpPager'   , sel: '#cmp-pager'   },
			selected: {name: 'UC.CmpSelected', sel: '#cmp-selected'},
			filter:   {name: 'UC.CmpFilter'  , sel: '#cmp-filter'  },
			content:  {name: 'UC.CmpContent' , sel: '#cmp-content' }
		},
		url: new URL('index.php', document.URL),				
		baseParams: {}
	});
	cmps = {
		sorter:   null,
		pager:    null,
		selected: null,
		filter:   null,
		content:  null
	};

	constructor(root, o = {})
	{
		super(root, o);
		
		// Object.assign(this.o, o); 
		this.o = _.merge({}, this.o, o); // must redo, because defaults applied after super(), weird
	}

	
	resetPage()
	{
		this.cmps.pager.reset();
	}

	
	onUserPagerChange(page)
	{
		// console.log(`pager - user change, to:`, page);

		this.loadContent();
	}
	
	onUserSorterChange(sorting)
	{
		// console.log(`sorter - user change, to:`, sorting);

		this.resetPage();

		this.loadContent();
	}

					
	loadContentTimeoutId = null;
	loadContentDelayed()
	{
		if (this.loadContentTimeoutId)
		{
			window.clearTimeout(this.loadContentTimeoutId);
			this.loadContentTimeoutId = null;
		}
		
		
		// Delayed load
		
		this.loadContentTimeoutId = window.setTimeout
		(
			this.loadContent.bind(this),
			800
		);
	}

	onThisFilterChange()
	{
		this.resetPage();
				
		this.loadContentDelayed();
	}
	
	
	filterUnset(id)
	{
		let cmps = this.cmps;
		
		cmps.selected.remove(id);
		cmps.filter.unset(id);

		this.onThisFilterChange();
	}
	
	filterReset()
	{
		let cmps = this.cmps;
		
		cmps.selected.reset();
		cmps.filter.reset();
		
		this.onThisFilterChange();
	}

	
	onUserSelectedRemove(id, el)
	{
		// console.log(`selected - user remove`);
		
		this.filterUnset(id);
	}
	
	onUserSelectedReset()
	{
		// console.log(`selected - user reset`);

		this.filterReset();
	}

	
	updateSelected(el) // override in derived classes
	{
		let cmps = this.cmps, csel = cmps.selected;
			
		switch (el.type)
		{
			case 'checkbox':
				el.checked ? csel.add(el.id, el.value) : csel.remove(el.id); 
			break;
			
			case 'radio':
				el.checked ? csel.add(el.name, el.value) : csel.remove(el.name); 
			break;

			case 'text':
				if (el.value !== '') csel.add(el.id, el.value); else csel.remove(el.id);
			break;
			
			case 'select-one':
				let i;
				if ((i = el.selectedIndex) != -1)
				{
					let title = el.options[i].text;
					csel.add(el.id, title);
				}
				else
				{
					csel.remove(el.id);
				}
			break;
			
			default:
				
		}
	}
	
	onUserFilterChange(el)
	{
		// console.log(`filter - user change`);
		
		this.updateSelected(el);
		// console.log(`user change`, el.name, el.value);

		this.onThisFilterChange();
	}
	/*
	onUserFilterSubmit()
	{
		console.log(`filter - user submit`);
	}
	*/
	onUserFilterReset()
	{
		// console.log(`filter - user reset`);

		this.filterReset();
	}
	
	onFilterSubmit()
	{
		// console.log(`filter - submit`);
		
		this.loadContent();
	}
	/*
	onFilterReset()
	{
		console.log(`filter - reset`);
	}
	*/
	

	
	beforeLoadContent()
	{
		return true;
	}
	
	afterLoadContent(data)
	{
		// console.log('data loaded');
		// console.log('loaded data: ', data);

		let cmps = this.cmps, ctnt = cmps.content;

		ctnt.updateView(data.html);
	}

	
	getURL()
	{
		return this.o.url;
	}
	
	buildSearchParamsByStage(stage, sp)
	{
		const cmps = this.cmps;
		
		switch (stage)
		{
			case 'addsorting':
				var cmp = cmps.sorter;
				let sorting = cmp.data.sorting;
				
				sp.append('sf', sorting.name);
				sp.append('so', sorting.dir);
			break;
			case 'addpage':
				var cmp = cmps.pager, page;
				
				if (cmp && (page = cmp.data.page)) sp.append('page', page);
			break;
			case 'addbase':
				for (const [k, v] of Object.entries(this.o.baseParams))
				{
					sp.append(k, v);
				}
			break;
			default:
				throw `Unsupported stage '${stage}'`;
		}
	}
	
	buildSearchParams()
	{
		let cmps = this.cmps, cfil = cmps.filter;
		
		let sp = new URLSearchParams(cfil.data.sp);
		
		this.buildSearchParamsByStage('addsorting', sp);
		this.buildSearchParamsByStage('addpage', sp);
		this.buildSearchParamsByStage('addbase', sp);
		
		return sp;
	}

	loadContent()
	{
		if (false === this.beforeLoadContent()) return;
		
		let cmps = this.cmps, cfil = cmps.filter, cpgr = cmps.pager, csrt = cmps.sorter;

		// let urlbase = document.URL.substr(0,document.URL.lastIndexOf('/')) + '/';
		// let urlbase = document.URL;
		// let url = new URL('index.php', urlbase);
		// let url = this.o.url;
		let url = this.getURL();

		let sp = this.buildSearchParams();
		
		url.search = sp;

		
		fetch(url)
		.then(response => response.json())
		.then(this.afterLoadContent.bind(this));
	}

	
	initSorter()
	{
		let cmps = this.cmps, clss = this.o.classes, cls = clss.sorter;
		
		(cmps.sorter = this.inst(cls.name, UC.$0(cls.sel),
			{
				onUserChange: this.onUserSorterChange.bind(this),
			}
		)).init();
	}
	
	initPager()
	{
		let cmps = this.cmps, clss = this.o.classes, cls = clss.pager;
		
		(cmps.pager = this.inst(cls.name, UC.$0(cls.sel),
			{
				onUserChange: this.onUserPagerChange.bind(this),
			}
		)).init();
	}
	
	initSelected()
	{
		let cmps = this.cmps, clss = this.o.classes, cls = clss.selected;
		
		(cmps.selected = this.inst(cls.name, UC.$0(cls.sel),
			{
				onUserRemove: this.onUserSelectedRemove.bind(this),
				onUserReset:  this.onUserSelectedReset.bind(this)
			}
		)).init();
	}
	
	initFilter()
	{
		let cmps = this.cmps, clss = this.o.classes, cls = clss.filter;
		
		(cmps.filter = this.inst(cls.name, UC.$0(cls.sel),
			{
				onUserChange: this.onUserFilterChange.bind(this),
				// onUserSubmit: this.onUserFilterSubmit.bind(this),
				onUserReset:  this.onUserFilterReset.bind(this),
				// onChange: this.onFilterChange.bind(this),
				onSubmit: this.onFilterSubmit.bind(this)
				// onReset:  this.onFilterReset.bind(this)
			}
		)).init();
	}
	
	initContent()
	{
		let cmps = this.cmps, clss = this.o.classes, cls = clss.content;
		
		(cmps.content = this.inst(cls.name, UC.$0(cls.sel))).init();
	}

	init()
	{
		this.initFilter();
		this.initSelected();
		this.initSorter();
		this.initContent();
		this.initPager();
	}
};
